@extends('layouts.app')

@section('content')

<div class="page events">

    <!-- form area -->
    <div class="row">
        <div class="small-10 small-offset-1 medium-6 medium-offset-3">
            <h1>Events</h1>
            @include('common.errors')

            @include('events.form')

            @if (count($events) > 0)
                @each('events.item', $events, 'event')
            @else
                <p>There are currently no events</p>
            @endif
        </div>
    </div>

</div>

@endsection
