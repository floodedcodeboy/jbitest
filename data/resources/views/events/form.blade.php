        @if (!Auth::guest())
            <div class="callout secondary">
                <h5>Create Event</h5>
                <form role="form" method="POST" action="{{ url('event') }}">
                {!! csrf_field() !!}

                <!-- title -->
                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            Title
                            <input type="text" name="title" value="{{ old('title') }}" aria-describedby="titleErrorText">
                        </label>
                         @if ($errors->has('title'))
                            <p class="help-text" id="titleErrorText">
                                <strong>{{ $errors->first('title') }}</strong>
                            </p>
                        @endif
                    </div>
                </div>

                <!-- description -->
                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            Description
                            <textarea name="description" aria-describedby="descErrorText">{{ old('description') }}</textarea>
                        </label>
                         @if ($errors->has('description'))
                            <p class="help-text" id="descErrorText">
                                <strong>{{ $errors->first('description') }}</strong>
                            </p>
                        @endif
                    </div>
                </div>

                <!-- price -->
                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            Price
                            <input type="text" name="price" value="{{ old('price') }}" aria-describedby="priceErrorText">
                        </label>
                         @if ($errors->has('price'))
                            <p class="help-text" id="priceErrorText">
                                <strong>{{ $errors->first('price') }}</strong>
                            </p>
                        @endif
                    </div>
                </div>

                <!-- start & end dt -->
                <div class="row">
                    <div class="small-12 medium-6 columns">
                        <label>
                            Start Date & Time
                            <input type="datetime" name="start_dt" value="{{ old('start_dt') }}" aria-describedby="startErrorText">
                        </label>

                            <p class="help-text" id="startErrorText">
                                Format: YYYY-MM-DD HH:MM:SS
                                @if ($errors->has('start_dt'))
                                <br/>
                                <strong>{{ $errors->first('start_dt') }}</strong>
                                @endif
                            </p>

                    </div>

                    <div class="small-12 medium-6 columns">
                        <label>
                            End Date & Time
                            <input type="datetime" name="end_dt" value="{{ old('end_dt') }}" aria-describedby="endErrorText">
                        </label>

                            <p class="help-text" id="endErrorText">
                                Format: YYYY-MM-DD HH:MM:SS
                                @if ($errors->has('end_dt'))
                                <br/>
                                <strong>{{ $errors->first('end_dt') }}</strong>
                                @endif
                            </p>

                    </div>
                </div>
                <!-- categories -->
                <div class="row">
                    <fieldset class="small-12 columns">
                        <legend>Categories</legend>
                        @if (count($categories) > 0)
                            @foreach ($categories as $category)
                                <input id="checkbox-{{ $category->id }}" type="checkbox" name="categories[{{ $category->id }}]"><label for="checkbox-{{ $category->id }}">{{ $category->name }}</label>
                            @endforeach
                        @else
                            <p>Please create some categories.</p>
                        @endif
                    </fieldset>
                </div>

                <!-- published -->
                <div class="row">
                    <fieldset class="small-12 columns">
                        <label>
                            <input type="checkbox" name="published" id="published"><label for="published">Publish?</label>
                        </label>
                    </fieldset>
                </div>

                <div class="row">
                    <div class="small-12 columns">
                        <button type="submit" class="button">Create Event</button>
                    </div>
                </div>
                </form>
            </div>
        @endif