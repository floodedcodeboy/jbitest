                    <div class="row event-item">
                        <div class="small-12">
                            <h4>{{ $event->title }}</h4>
                            <h5>{{ date('d F, Y H:i', strtotime($event->start_dt)) }} - {{ date('d F, Y H:i', strtotime($event->end_dt)) }}</h5>
                            <h6>£{{ $event->price }}</h6>
                            <p>{{ $event->description }}</p>
                            @if (!Auth::guest())
                            <form action="{{ url('event/'.$event->id) }}" method="POST">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button type="submit" id="delete-event-{{ $event->id }}" class="alert button">Delete</button>
                            </form>
                            @endif
                            @if (count($event->categories) > 0)
                                @foreach ($event->categories as $category)
                                    <span class="success label">{{ $category->name }}</span>
                                @endforeach
                            @endif

                            <hr>
                        </div>
                    </div>