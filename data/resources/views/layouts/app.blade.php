<!DOCTYPE html>
<!--

            88  8888888888888888~    $$
            88  88             888   $$
            88  88              88?
            88  88              Z87  88
            88  88             :88   88
            88  88??????????OO888    88
            88  8888888888888888$    88
            88  88             788+  88
            88  88               88  88
            88  88               88  88
88         I88  88               88  88
$8O       :88   88             888,  88
 ~8888888888    8888888888888888+    88
    +888$,      ~~~~~~~~~~~~         ~~
-->

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>JBiDigital Test</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/foundation.css') }}" />

</head>
<body id="app-layout">

    <div class="top-bar">
        <div id="responsive-menu">
            <div class="top-bar-left">
                <img src="http://www.jbidigital.co.uk/wp-content/themes/jbi/img/goldfinger/header-logo-white.png" alt="jbi digital" class="hide-for-small-only"/>
            </div>
            <div class="top-bar-right">
                @if (Auth::guest())
                <ul class="menu">
                    <li class="show-for-small-only"><img src="http://www.jbidigital.co.uk/wp-content/themes/jbi/img/goldfinger/header-logo-white.png" alt="jbi digital"/></li>
                    <li><a href="{{ url('/events') }}">Events</a></li>
                    <li><a href="{{ url('/categories') }}">Categories</a></li>
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>

                </ul>
                @else
                <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="{{ url('/events') }}">Events</a></li>
                    <!-- <li><a href="{{ url('/venues') }}">Venues</a></li> -->
                    <li><a href="{{ url('/categories') }}">Categories</a></li>
                    <li class="has-submenu">
                        <a href="#">{{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="menu vertical">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                </ul>
                @endif
            </div>
        </div>
    </div>

    @yield('content')

    <!-- JavaScripts -->
    <script src="{{ elixir('js/foundation.js') }}"></script>
</body>
</html>
