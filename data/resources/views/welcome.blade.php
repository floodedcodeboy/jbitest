@extends('layouts.app')

@section('content')

<div class="page welcome">
    <div class="row">
        <div class="small-10 small-offset-1 medium-6 medium-offset-3">
            <h1>Welcome</h1>
            <p>This is a small test provided by JBiDigital to Jacob Carthew-Gabriel.</p>
            <p>Disclaimer: All code and related decisions here are my own.</p>
        </div>
    </div>
</div>

@endsection
