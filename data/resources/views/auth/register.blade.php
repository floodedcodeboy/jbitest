@extends('layouts.app')

@section('content')

<div class="page register">
    <div class="row">
        <div class="small-10 small-offset-1 medium-6 medium-offset-3">
            <h1>Register</h1>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
            {!! csrf_field() !!}
                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            Name
                            <input type="text" name="name" value="{{ old('name') }}" aria-describedby="nameField">
                        </label>
                        @if ($errors->has('name'))
                        <p class="help-text" id="nameField">
                            <strong>{{ $errors->first('name') }}</strong>
                        </p>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            E-Mail Address
                            <input type="email" aria-describedby="emailField" name="email" value="{{ old('email') }}">
                        </label>
                        @if ($errors->has('email'))
                        <p class="help-text" id="emailField">
                            <strong>{{ $errors->first('email') }}</strong>
                        </p>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            Password
                            <input type="password" aria-describedby="passwordField" name="password">
                        </label>
                        @if ($errors->has('password'))
                        <p class="help-text" id="passwordField">
                            <strong>{{ $errors->first('password') }}</strong>
                        </p>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            Confirm Password
                            <input type="password" aria-describedby="confirmPassword" name="password_confirmation">
                        </label>
                        @if ($errors->has('password_confirmation'))
                        <p class="help-text" id="confirmPassword">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </p>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="small-12 columns">
                        <button type="submit" class="button">Register</button>
                    </div>
                </div>


            </form>

        </div>
    </div>
</div>
@endsection
