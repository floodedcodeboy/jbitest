@extends('layouts.app')

<!-- Main Content -->
@section('content')

<div class="page welcome">
    <div class="row">
        <div class="small-10 small-offset-1 medium-6 medium-offset-3">
            <h1>Reset Password</h1>
            @if (session('status'))
            <div class="callout alert">
                {{ session('status') }}
            </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
            {!! csrf_field() !!}
            <div class="row">
                <div class="small-12 columns">
                    <label>
                        E-Mail Address
                        <input type="email" name="email" value="{{ old('email') }}" aria-describedby="emailErrorText">
                    </label>
                     @if ($errors->has('email'))
                        <p class="help-text" id="emailErrorText">
                            <strong>{{ $errors->first('email') }}</strong>
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <button type="submit" class="button">Send Password Reset Link</button>
                </div>
            </div>
            </form>

        </div>
    </div>
</div>
@endsection
