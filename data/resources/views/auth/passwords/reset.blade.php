@extends('layouts.app')

@section('content')
<div class="page reset">
    <div class="row">
        <div class="small-10 small-offset-1 medium-6 medium-offset-3">
            <h1>Reset Password</h1>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
            {!! csrf_field() !!}
            <input type="hidden" name="token" value="{{ $token }}">

                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            E-mail Address
                            <input type="email" name="email" value="{{ $email or old('email') }}" aria-describedby="emailField">
                            @if ($errors->has('email'))
                                <p class="help-text" id="emailField">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </p>
                            @endif
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            Password
                            <input type="password" name="password" aria-describedby="passwordField">
                            @if ($errors->has('password'))
                                <p class="help-text" id="passwordField">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </p>
                            @endif
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            Confirm Password
                            <input type="password" name="password_confirmation" aria-describedby="passwordConfirmation">
                            @if ($errors->has('password_confirmation'))
                                <p class="help-text" id="passwordConfirmation">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </p>
                            @endif
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <button type="submit" class="button">Reset Password</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
@endsection
