@extends('layouts.app')

@section('content')

<div class="page login">

    <div class="row">
        <div class="small-10 small-offset-1 medium-6 medium-offset-3">
            <h1>Login</h1>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}
            <div class="row">
                <div class="small-12 columns">
                    <label>
                        E-Mail Address
                        <input type="email" name="email" value="{{ old('email') }}" aria-describedby="emailErrorText">
                    </label>
                     @if ($errors->has('email'))
                        <p class="help-text" id="emailErrorText">
                            <strong>{{ $errors->first('email') }}</strong>
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="small-12 columns">
                    <label>
                        Password
                        <input type="password" name="password" aria-describedby="passwordErrorText">
                    </label>
                     @if ($errors->has('password'))
                        <p class="help-text" id="passwordErrorText">
                            <strong>{{ $errors->first('password') }}</strong>
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <fieldset class="small-12 columns">
                    <label>
                        <input type="checkbox" name="remember" id="rememberme"><label for="rememberme">Remember Me</label>
                    </label>
                </fieldset>
            </div>

            <div class="row">
                <div class="small-12 columns">
                    <button type="submit" class="button">Login</button>
                    <p class="help-text"><a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a></p>
                </div>
            </div>


        </form>
        </div>
    </div>

</div>

@endsection
