                    <div class="row category-item">
                        <div class="small-12 medium-2 columns">
                            <h6>{{ $category->name }}</h3>
                            @if (!Auth::guest())
                            <form action="{{ url('category/'.$category->id) }}" method="POST">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button type="submit" id="delete-category-{{ $category->id }}" class="alert button">Delete</button>
                            </form>
                            @endif
                        </div>

                        <div class="small-12 medium-10 columns">
                            @if (count($category->events) > 0)
                                <h3>Events</h3>
                                @foreach ($category->events as $event)
                                    <div class="callout secondary">
                                        <h4>{{ $event->title }}</h4>
                                        <h5>{{ date('d F, Y H:i', strtotime($event->start_dt)) }} - {{ date('d F, Y H:i', strtotime($event->end_dt)) }}</h5>
                                        <h6>£{{ $event->price }}</h6>
                                        <p>{{ $event->description }}</p>
                                    </div>

                                @endforeach
                            @endif
                        </div>
                        <hr>
                    </div>