@extends('layouts.app')

@section('content')

<div class="page categories">

    <!-- form area -->
    <div class="row">
        <div class="small-10 small-offset-1 medium-6 medium-offset-3">
            <h1>Categories</h1>

            @include('categories.form')

            @if (count($categories) > 0)
                @each('categories.item', $categories, 'category')
            @else
                <p>There are currently no categories</p>
            @endif
        </div>
    </div>

</div>

@endsection
