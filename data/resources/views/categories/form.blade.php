        @if (!Auth::guest())
            <div class="callout secondary">
                <h5>Create Category</h5>
                <form role="form" method="POST" action="{{ url('category') }}">
                {!! csrf_field() !!}

                <!-- title -->
                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            Name
                            <input type="text" name="name" value="{{ old('name') }}" aria-describedby="nameErrorText">
                        </label>
                         @if ($errors->has('name'))
                            <p class="help-text" id="nameErrorText">
                                <strong>{{ $errors->first('name') }}</strong>
                            </p>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="small-12 columns">
                        <button type="submit" class="button">Create Category</button>
                    </div>
                </div>
                </form>
            </div>
        @endif