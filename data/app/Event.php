<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    //

    public function categories() {
        return $this->belongsToMany('App\Category')->withTimestamps();
    }

    public function venue() {
        // get venue associated to the event`
    }
}
