<?php
namespace App\Repositories;

use App\Category;
use App\Event;
use App\Venue;
use App\User;

class EventRepository
{
    /**
     * Get all of the published events
     *
     * @return Collection
     */
    public function forAdminUser(User $user) {
        $results = Event::with('categories')
                    ->orderBy('start_dt', 'asc')
                    ->get();

        return $results;
    }

    public function forUser() {
        return Event::where('published', 1)
                    ->orderBy('start_dt', 'asc')
                    ->get();
    }

    /*
    public function byCategory(Category $category) {
        //return Event::where()
    }

    public function byVenue(Venue $venue) {
        return Event::where('venue_id', $venue->id)
                    ->orderBy('start_dt', 'asc')
                    ->get();
    }
    */
}