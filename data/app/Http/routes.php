<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

// EVENTS
Route::get('/events', 'EventController@index');
Route::post('/event', 'EventController@store');
Route::delete('/event/{event}', 'EventController@remove');

// CATEGORIES
Route::get('/categories', 'CategoryController@index');
Route::post('/category', 'CategoryController@store');
Route::delete('/category/{category}', 'CategoryController@remove');