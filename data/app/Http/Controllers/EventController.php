<?php

namespace App\Http\Controllers;

use App\Event;
use App\Venue;
use App\Category;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\EventRepository;

class EventController extends Controller
{
    //
    protected $events;

    public function __construct(EventRepository $events) {
        $this->middleware('auth', ['except' => ['index']]);
        $this->events = $events;
    }

    /**
     * Display a list of all of the events
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request) {

        if (is_null($request->user())) {
            return view('events.index', [
                'events' => $this->events->forUser()
            ]);
        } else {
            // grab all the categories so we can populate a list of them for the form
            $categories = Category::all()->sortBy('name');
            return view('events.index', [
                'events' => $this->events->forAdminUser($request->user()),
                'categories' => $categories
            ]);
        }
    }

    /**
     * Create a new event.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required',
            'price' => 'required',
            'start_dt' => 'required',
            'end_dt' => 'required'
        ]);

        // create the event
        $event = new Event;
        $event->title = $request->title;
        $event->description = $request->description;
        $event->venue_id = $request->venue_id;
        $event->price = $request->price;
        $event->start_dt = $request->start_dt;
        $event->end_dt = $request->end_dt;
        $event->published = $request->published;
        $event->save();

        $categories = array_keys($request->categories);

        $event->categories()->sync($categories);

        return redirect('/events');
    }

    /**
     * Delete the given event.
     *
     * @param  Request  $request
     * @param  Event  $event
     * @return Response
     */
    public function remove(Request $request, Event $event)
    {
        $event->categories()->detach();
        $event->delete();
        // TODO ensure delete of CategoryTasks entry
        return redirect('/events');
    }
}
