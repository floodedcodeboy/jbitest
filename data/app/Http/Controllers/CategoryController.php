<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Task;
use App\Category;

class CategoryController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => ['index']]);

    }

    /**
     * Display a list of all of the events
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request) {
        $categories = Category::all()->sortBy('name');
        return view('categories.index', [
                'categories' => $categories
            ]);
    }

    /**
     * Create a new categrory.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        // create the category
        $category = new Category;
        $category->name = $request->name;

        $category->save();

        return redirect('/categories');
    }

    /**
     * Delete the category.
     *
     * @param  Request  $request
     * @param  Category  $category
     * @return Response
     */
    public function remove(Request $request, Category $category)
    {
        // TODO check that there are no events attached to the category
        $category->delete();
        return redirect('/categories');
    }
}
