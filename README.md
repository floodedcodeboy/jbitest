# JBIDigital Test 

## SPEC 
provided by JBiDigital
#### CREATING A CUSTOM MODULE

For this exercise, we would like you to set up a basic site and create a Custom module for an event section with the following features:

* The ability for Admin to create Event and Event Categories.
* Front end – List all events, grouped by category.
* The ability to subscribe to the event. All event subscribers should be stored in DB and listed to the Admin. (Optional)

You may assume the relevant fields and attributes.

## BUILD 

#### SYSTEM REQUIREMENTS

* Vagrant
* Virtualbox
* Node
* Bower
* Gulp

#### DEVELOPMENT ENVIRONMENT

Vagrant, provisioned through Ansible 

* Ubuntu: 14.04.4
* Apache2: 2.4.7
* MySQL: 5.5.47
* PHP: 5.5.34


Framework : Laravel 5.2

#### RUNNING THE PROJECT

##### Getting Setup


###### Install Gulp

run `npm install --g gulp` to install Gulp globally

###### Install Node Dependancies

run `npm install`



From this folder run: `vagrant up` from the command line. 
Your vagrant server will be created, provisioned and ready to use.

Add the following line to your hosts file so that you have a pretty url to access the site on: `192.168.33.99 jbibuild.dev`

Then run `vagrant ssh` to log in to the server.

Then navigate to your project root: `cd /vagrant/`
Then run `composer install`
Then run the migrations `php artisan migrate`

Now the project is ready to use. 

You can visit `jbibuild.dev` in your browser.

In order to access the 'admin' side of things you will have to register on the site.

Once you have registered, you can login and add categories and events. 


### Notes 

All the steps after `vagrant up` could be automated. 


### Thank you for your time.





